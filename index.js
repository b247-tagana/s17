/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let promptName = prompt('What is your name?');
	let promptAge = prompt('How old are you?');
	let promptLocation = prompt('Where do you live?');
	console.log('Hello, ' + promptName);
	console.log('You are ' + promptAge + ' years old.');
	console.log('You live in ' + promptLocation);

/*	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.*/
	


	//second function here:
	let promptBandOne = prompt('Top 5 favorite bands(1)?')
	let promptBandTwo = prompt('Top 5 favorite bands(2)?')
	let promptBandThree = prompt('Top 5 favorite bands(3)?')
	let promptBandFour = prompt('Top 5 favorite bands(4)?')
	let promptBandFive = prompt('Top 5 favorite bands(5)?')
	console.log('1. ' + promptBandOne);
	console.log('2. ' + promptBandTwo);
	console.log('3. ' + promptBandThree);
	console.log('4. ' + promptBandFour);
	console.log('5. ' + promptBandFive);

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let promptMovieOne = prompt('Top 5 favorite movies(1)?')
	let promptMovieTwo = prompt('Top 5 favorite movies(2)?')
	let promptMovieThree = prompt('Top 5 favorite movies(3)?')
	let promptMovieFour = prompt('Top 5 favorite movies(4)?')
	let promptMovieFive = prompt('Top 5 favorite movies(5)?')
	console.log('1. ' + promptMovieOne);
	console.log('Rotten Tomatoes Rating: 81%')
	console.log('2. ' + promptMovieTwo);
	console.log('Rotten Tomatoes Rating: 90%')
	console.log('3. ' + promptMovieThree);
	console.log('Rotten Tomatoes Rating: 71%')
	console.log('4. ' + promptMovieFour);
	console.log('Rotten Tomatoes Rating: 95%')
	console.log('5. ' + promptMovieFive);
	console.log('Rotten Tomatoes Rating: 75%')

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
printUsers();
	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};